##!/bin/bash

# Need this to checkout the puppet repo
rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

touch /etc/puppet/hiera.yaml

puppet module install puppetlabs-git
puppet module install jfryman-nginx
puppet module install jfryman-selinux

yum install -y git

rm -rf puppet-nginx-exercise

git clone https://bitbucket.org/nfowlie/puppet-nginx-exercise.git