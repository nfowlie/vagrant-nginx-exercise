# README #


### What is this repository for? ###

This repo contains the vagrant file & associated bootstrap script required to deploy the PuppetLabs nginx exercise. It will stand up a Centos7 box running nginx, and a single html page hosted on port 8000.

### How do I get set up? ###

* Install Vagrant.
* Clone this repository
* From the repository root, execute `vagrant up`
* Open up your browser and goto http://<ip of vagrant box>:8000